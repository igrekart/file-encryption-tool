package ci.yoru.uplaoder.Controllers;

import ci.yoru.uplaoder.Contracts.GetFileStorageDto;
import ci.yoru.uplaoder.Contracts.PostFileResponseDto;
import ci.yoru.uplaoder.Exceptions.ApplicationException;
import ci.yoru.uplaoder.Exceptions.BadFileUploadedException;
import ci.yoru.uplaoder.Exceptions.ExceptionResponse;
import ci.yoru.uplaoder.Exceptions.FIleUploadedNotFoundException;
import ci.yoru.uplaoder.Services.FileStorageService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.swing.text.DateFormatter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;


@RestController
public class FileStorageController extends DefaultController{

    @Autowired
    FileStorageService service;

    @PostMapping(value = "/add", consumes = {"multipart/form-data"}, produces = {"application/json"})
    public PostFileResponseDto postFile(@RequestParam("file") MultipartFile file) throws Exception{

        if(file == null || file.getBytes().length == 0  || file.isEmpty()){
            throw new BadFileUploadedException("Le fichier uploader contient des erreurs");
        }
        var fileStorage = service.addNewFile(file);
        return PostFileResponseDto
                .builder()
                .id(fileStorage.getId())
                .fileEncoded(fileStorage.getFileEncoded())
                .filename(fileStorage.getFilename())
                .createdAt(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(fileStorage.getCreatedAt()))
                .build();
    }

    @GetMapping(value = "/download/{id}", produces = {"application/json"})
    public ResponseEntity<Object> getFile(@NonNull @PathVariable UUID id) throws FIleUploadedNotFoundException, IOException {
        return service.getFileStorage(id);
    }
}
