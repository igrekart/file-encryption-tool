package ci.yoru.uplaoder.Controllers;

import ci.yoru.uplaoder.Exceptions.ApplicationException;
import ci.yoru.uplaoder.Exceptions.BadFileUploadedException;
import ci.yoru.uplaoder.Exceptions.ExceptionResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {
    @ExceptionHandler({ApplicationException.class})
    public ExceptionResponse listerUtilisateurException(ApplicationException exception) {
        return ExceptionResponse
                .builder()
                .message(exception.getMessage())
                .code_message(exception.getCode_message())
                .code(exception.getCode())
                .build();
    }
}
