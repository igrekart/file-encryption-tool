package ci.yoru.uplaoder.Contracts;


import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
public class PostFileResponseDto {
    private UUID id;
    private String filename;
    private String fileEncoded;
    private String createdAt;
}
