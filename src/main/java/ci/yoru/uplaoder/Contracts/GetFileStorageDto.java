package ci.yoru.uplaoder.Contracts;

import lombok.Getter;

import java.util.UUID;

@Getter
public class GetFileStorageDto {
    private UUID id;
    private String filename;
    private String fileEncoded;
    private String contentType;
    private String createdAt;
}
