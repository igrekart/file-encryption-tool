package ci.yoru.uplaoder.Entities;


import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity(name = "file_storage")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileStorage {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "filename", nullable = false, length = 100)
    private String filename;

    @Column(name = "file_encoded", nullable = false)
    @Lob
    private String fileEncoded;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @Column(name = "content_type", nullable = false, length = 50)
    private String contentType;

    @PrePersist
    public void prePersist() {
        if (createdAt == null) {
            createdAt = LocalDateTime.now();
        }
    }
}
