package ci.yoru.uplaoder.Utils;

import org.springframework.core.io.InputStreamSource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Encoder {
    String encode(InputStreamSource file) throws IOException;
    InputStreamSource decode(String file) throws IOException;
}
