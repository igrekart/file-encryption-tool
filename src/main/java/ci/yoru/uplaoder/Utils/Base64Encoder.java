package ci.yoru.uplaoder.Utils;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Base64;

@Component
public class Base64Encoder implements Encoder{
    @Override
    public String encode(InputStreamSource file) throws IOException {
        byte[] bites = file.getInputStream().readAllBytes();
        return Base64.getEncoder().encodeToString(bites);
    }

    @Override
    public InputStreamSource decode(String file) throws IOException {
        byte[] byteArray = Base64.getDecoder().decode(file);
        // Create ByteArrayInputStream from the decoded byte array
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);
        return new InputStreamResource(byteArrayInputStream);
    }

    private static void writeToOutputStream(String base64String, OutputStream outputStream) throws IOException {
        // Decode Base64 string to byte array
        byte[] byteArray = Base64.getDecoder().decode(base64String);

        // Create ByteArrayInputStream from the decoded byte array
        try (InputStream inputStream = new ByteArrayInputStream(byteArray)) {
            // Copy data from the ByteArrayInputStream to the OutputStream
            copy(inputStream, outputStream);
        }
    }

    private static void copy(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
}
