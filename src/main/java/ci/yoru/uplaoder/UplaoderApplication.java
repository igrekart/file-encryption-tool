package ci.yoru.uplaoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UplaoderApplication {
    public static void main(String[] args) {
        SpringApplication.run(UplaoderApplication.class, args);
    }
}
