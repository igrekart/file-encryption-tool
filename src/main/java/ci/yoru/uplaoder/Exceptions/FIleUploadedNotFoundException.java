package ci.yoru.uplaoder.Exceptions;

import lombok.Getter;

@Getter
public class FIleUploadedNotFoundException extends ApplicationException{

    protected static final int code = 403;
    protected static final String code_message = "FILE_NOT_FOUND";

    FIleUploadedNotFoundException(){
        super();
    }
    public FIleUploadedNotFoundException(String message){
        super(message);
    }
}
