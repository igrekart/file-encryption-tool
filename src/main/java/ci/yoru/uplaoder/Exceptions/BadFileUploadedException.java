package ci.yoru.uplaoder.Exceptions;

import lombok.Getter;

@Getter
public class BadFileUploadedException extends ApplicationException{

    protected static final int code = 404;
    protected static final String code_message = "BAD_REQUEST";

    BadFileUploadedException(){
        super();
    }
    public BadFileUploadedException(String message){
        super(message);
    }
}
