package ci.yoru.uplaoder.Exceptions;

import lombok.Getter;

@Getter
public class ApplicationException extends Exception{

    protected final int code = 400;
    protected final String code_message = "BAD_REQUEST";

    ApplicationException(){
        super();
    }
    public ApplicationException(String message){
        super(message);
    }
}
