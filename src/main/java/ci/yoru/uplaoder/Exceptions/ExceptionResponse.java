package ci.yoru.uplaoder.Exceptions;

import lombok.Builder;
import lombok.Getter;

import javax.swing.*;

@Getter
@Builder
public class ExceptionResponse {
    private String message;
    private int code;
    private String code_message;
}
