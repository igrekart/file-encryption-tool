package ci.yoru.uplaoder.Services;

import ci.yoru.uplaoder.Entities.FileStorage;
import ci.yoru.uplaoder.Exceptions.FIleUploadedNotFoundException;
import ci.yoru.uplaoder.Repositories.FileStorageRepository;
import ci.yoru.uplaoder.Utils.Base64Encoder;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class FileStorageService {

    @Autowired
    FileStorageRepository repository;
    @Autowired Base64Encoder encoder;
    public FileStorage addNewFile(@NonNull MultipartFile file) throws IOException {
         return repository.save(FileStorage
               .builder()
               .filename(file.getOriginalFilename())
               .fileEncoded(encoder.encode(file))
               .contentType(file.getContentType())
               .build())
         ;
    }

    public ResponseEntity<Object> getFileStorage(@NonNull UUID id) throws FIleUploadedNotFoundException, IOException {
        var fileStorage = repository.findById(id).orElseThrow(() -> new FIleUploadedNotFoundException("File not found"));
        InputStreamSource resource = encoder.decode(fileStorage.getFileEncoded());
        return ResponseEntity
                .ok()
                .headers(new HttpHeaders())
                .contentType(MediaType.parseMediaType(fileStorage.getContentType()))
                .body(resource);
    }
}
